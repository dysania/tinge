EXTENSION_UUID="tinge@dysania.gitlab.com"
GNOME_SHELL_EXTENSIONS=~/.local/share/gnome-shell/extensions
INSTALL_PATH=$(GNOME_SHELL_EXTENSIONS)/$(EXTENSION_UUID)

install: $(INSTALL_PATH) gschemas
	cd src && cp -a . $(INSTALL_PATH)
.PHONY: install

bundle: dist gschemas
	cd src && zip -r ../dist/$(EXTENSION_UUID).zip .
.PHONY: bundle

uninstall:
	rm -rf $(INSTALL_PATH)
.PHONY: uninstall

clean:
	rm -rf dist src/schemas/gschemas.compiled
.PHONY: clean

debug-prefs: install
	gnome-extensions reset $(EXTENSION_UUID)
	gnome-extensions enable $(EXTENSION_UUID)
	gnome-shell-extension-prefs $(EXTENSION_UUID)
.PHONY: debug-prefs

gschemas: src/schemas/gschemas.compiled
.PHONY: gschemas

dist:
	mkdir -p dist

src/schemas/gschemas.compiled: src/schemas/org.gnome.shell.extensions.tinge-the-screen.gschema.xml
	glib-compile-schemas src/schemas/

$(INSTALL_PATH):
	mkdir -p $(INSTALL_PATH)
