"use strict";

const { Clutter, GObject, Shell, St } = imports.gi;

const {
  main: Main,
  tweener: Tweener,
  panelMenu: PanelMenu,
  popupMenu: PopupMenu
} = imports.ui;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const UUID = Me.metadata.uuid;

let button = null;
let tinge_effect = null;

const TingeShader = GObject.registerClass(
  class TingeShader extends Clutter.ShaderEffect {
    /**
     * TingeShader constructor.
     *
     * @param {Clutter.Color} tinge The color to tinge the screen with
     */
    _init() {
      super._init({ shader_type: Clutter.ShaderType.FRAGMENT_SHADER });
      this._name = "tinge-dysania-gitlab-com";
      this._state = "disabled";
      this._tinge = null;
      this._alpha = 0;
      this._settings = ExtensionUtils.getSettings();

      this._settings.connect("changed", this.update_tinge.bind(this, true));
      this.update_tinge(true);

      // Brightness/Contrast calculation:
      // https://stackoverflow.com/a/50053219/1307440
      // https://www.dfstudios.co.uk/articles/programming/image-programming-algorithms/image-processing-algorithms-part-5-contrast-adjustment/
      this.set_shader_source(`
        uniform sampler2D tex;
        uniform int tinge_r;
        uniform int tinge_g;
        uniform int tinge_b;
        uniform int tinge_a;
        uniform float brightness_alpha;
        uniform float brightness_beta;
        uniform float contrast_alpha;
        uniform float contrast_beta;

        void main() {
          vec2 col = cogl_tex_coord_in[0].xy;
          cogl_color_out = texture2D(tex, col);

          vec3 tinge = vec3(tinge_r, tinge_g, tinge_b) / 255.0;
          cogl_color_out.rgb = mix(cogl_color_out.rgb, tinge, tinge_a / 255.0);

          cogl_color_out.rgb = (cogl_color_out.rgb * contrast_alpha) + contrast_beta;
          cogl_color_out.rgb = (cogl_color_out.rgb * brightness_alpha) + brightness_beta;

          cogl_color_out.rgb = min(
            max(
              cogl_color_out.rgb,
              vec3(0.0, 0.0, 0.0)
            ),
            vec3(1.0, 1.0, 1.0)
          );
        }
      `);
    }

    update_tinge(all) {
      if (all) {
        const settingsTinge = this._settings.get_string("tinge-color");
        const settingsAlpha = this._settings.get_int("tinge-alpha");
        const settingsContrast = Number(this._settings.get_int("contrast"));
        const settingsBrightness = Number(this._settings.get_int("brightness"));
        const [ok, tinge] = Clutter.Color.from_string(settingsTinge);

        if (!ok || isNaN(settingsContrast) || isNaN(settingsBrightness)) {
          return false;
        }

        log("Raw values:", settingsContrast, settingsBrightness);

        this._tinge = tinge;
        this._tinge.alpha = this._alpha = settingsAlpha;

        this.set_uniform_value("tinge_r", this._tinge.red);
        this.set_uniform_value("tinge_g", this._tinge.green);
        this.set_uniform_value("tinge_b", this._tinge.blue);

        // Brightness/Contrast calculation:
        // https://stackoverflow.com/a/50053219/1307440
        // https://www.dfstudios.co.uk/articles/programming/image-programming-algorithms/image-processing-algorithms-part-5-contrast-adjustment/

        let shadow;
        let highlight;

        if (settingsBrightness > 0) {
          shadow = settingsBrightness;
          highlight = 255;
        } else {
          shadow = 0;
          highlight = 255 + settingsBrightness;
        }

        let brightnessAlpha = +((highlight - shadow) / 255).toFixed(4);
        let brightnessBeta = +(shadow / 255).toFixed(4);

        let f =
          (259.0476 * (settingsContrast + 255)) /
          (255 * (259.0476 - settingsContrast));
        let contrastAlpha = +f.toFixed(4);
        let contrastBeta = +(0.5 * (1 - f)).toFixed(4);

        log("Brightness:", brightnessAlpha, brightnessBeta);
        log("Contrast:", contrastAlpha, contrastBeta);
        log("Alpha:", this.alpha);

        this.set_uniform_value("brightness_alpha", brightnessAlpha);
        this.set_uniform_value("brightness_beta", brightnessBeta);
        this.set_uniform_value("contrast_alpha", contrastAlpha);
        this.set_uniform_value("contrast_beta", contrastBeta);
      }

      this.set_uniform_value("tinge_a", this.alpha);
    }

    get alpha() {
      return this._alpha;
    }

    set alpha(value) {
      this._alpha = value;
      this.update_tinge();
    }

    enable() {
      if (this._state !== "disabled") {
        // early exit if already enabled
        return;
      }

      this._state = "enabling";
      this.alpha = 0;
      Main.uiGroup.add_effect_with_name(this._name, this);

      Tweener.addTween(this, {
        alpha: this._tinge.alpha,
        time: 0.2,
        transition: "easeOutQuad",
        onComplete: () => {
          this._state = "enabled";
        }
      });
    }

    disable() {
      if (this._state === "disabled" || this._state === "disabling") {
        // early exit if already disabled
        return;
      }

      this._state = "disabling";

      Tweener.addTween(this, {
        alpha: 0,
        time: 0.2,
        transition: "easeOutQuad",
        onComplete: () => {
          Main.uiGroup.remove_effect_by_name(this._name);
          this._state = "disabled";
        }
      });
    }

    toggle() {
      if (this._state === "enabled") {
        this.disable();
      } else if (this._state === "disabled") {
        this.enable();
      }
    }
  }
);

const TingeButton = GObject.registerClass(
  class TingeButton extends PanelMenu.Button {
    _init(tinge_effect) {
      super._init(St.Align.START);

      let box = new St.BoxLayout({ style_class: "panel-status-menu-box" });
      let icon = new St.Icon({
        icon_name: "applications-graphics-symbolic",
        style_class: "system-status-icon emotes-icon"
      });

      box.add(icon);
      box.add(PopupMenu.arrowIcon(St.Side.BOTTOM));
      this.add_child(box);

      this._toggle = new PopupMenu.PopupSwitchMenuItem(
        _("Enable Tinge"),
        false,
        { style_class: "popup-subtitle-menu-item" }
      );
      this._toggle.connect("toggled", tinge_effect.toggle.bind(tinge_effect));
      this.menu.addMenuItem(this._toggle);

      this._preferences = new PopupMenu.PopupMenuItem(_("Preferences"));
      this._preferences.connect("activate", this._onPreferences.bind(this));
      this.menu.addMenuItem(this._preferences);
    }

    _onPreferences() {
      let _appSys = Shell.AppSystem.get_default();
      let _gsmPrefs = _appSys.lookup_app("gnome-shell-extension-prefs.desktop");

      if (_gsmPrefs.get_state() === _gsmPrefs.SHELL_APP_STATE_RUNNING) {
        _gsmPrefs.activate();
      } else {
        let info = _gsmPrefs.get_app_info();
        let timestamp = global.display.get_current_time_roundtrip();

        info.launch_uris(
          [UUID],
          global.create_app_launch_context(timestamp, -1)
        );
      }
    }
  }
);

function init() {
  log(`Initializing ${Me.metadata.name}`);
}

function enable() {
  log(`Enabling ${Me.metadata.name}`);

  tinge_effect = new TingeShader();
  button = new TingeButton(tinge_effect);

  Main.panel.addToStatusArea("tingeTheScreen", button);

  // logAllProperties(shader_effect);
}

function disable() {
  log(`Disabling ${Me.metadata.name}`);

  tinge_effect.disable();
  button.destroy();
}

function logAllProperties(obj) {
  if (obj == null) return; // recursive approach
  log(Object.getOwnPropertyNames(obj));
  log(Object.keys(obj));

  logAllProperties(Object.getPrototypeOf(obj));
}
