"use strict";

const { Gdk, Gio, GObject, Gtk } = imports.gi;

// TODO: Replace this with Gettext translations
const _ = text => text;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

function init() {
  print(`Initializing ${Me.metadata.name} Preferences`);
}

const PrefsWidget = GObject.registerClass(
  class PrefsWidget extends Gtk.Grid {
    _init() {
      super._init({
        halign: Gtk.Align.CENTER,
        margin: 24,
        column_spacing: 12,
        row_spacing: 6,
        width_request: 500
      });

      this._settings = ExtensionUtils.getSettings();

      let currentTinge = this._settings.get_string("tinge-color");
      const rgba = new Gdk.RGBA();

      if (!rgba.parse(currentTinge)) {
        rgba.parse("#ffd");
      }

      let colorButton = new Gtk.ColorButton({
        rgba,
        use_alpha: false
      });

      colorButton.connect("color-set", () => {
        const settingsTinge = colorButton.get_rgba().to_string();
        this._settings.set_string("tinge-color", settingsTinge);
      });
      this._addRow(1, _("Tinge Color"), colorButton);

      let adjustment = this._createAdjustment("tinge-alpha", 1);
      let scale = new Gtk.Scale({ adjustment, draw_value: true, digits: 0 });
      this._addRow(2, _("Tinge Alpha"), scale);

      adjustment = this._createAdjustment("brightness", 1);
      scale = new Gtk.Scale({ adjustment, draw_value: true, digits: 0 });
      scale.add_mark(0, Gtk.PositionType.BOTTOM, null);
      this._addRow(3, _("Brightness"), scale);

      adjustment = this._createAdjustment("contrast", 1);
      scale = new Gtk.Scale({ adjustment, draw_value: true, digits: 0 });
      scale.add_mark(0, Gtk.PositionType.BOTTOM, null);
      this._addRow(4, _("Contrast"), scale);
    }

    _addRow(row, label, widget) {
      let margin = 48;

      widget.margin_end = margin;
      widget.hexpand = true;

      if (!this._sizeGroup) {
        this._sizeGroup = new Gtk.SizeGroup({
          mode: Gtk.SizeGroupMode.VERTICAL
        });
      }

      this._sizeGroup.add_widget(widget);

      this.attach(
        new Gtk.Label({
          label,
          xalign: 1, // right align
          margin_start: margin
        }),
        0,
        row,
        1,
        1
      );

      this.attach(widget, 1, row, 1, 1);
    }

    _createAdjustment(key, step) {
      let schemaKey = this._settings.settings_schema.get_key(key);
      let [type, variant] = schemaKey.get_range().deep_unpack();

      if (type !== "range")
        throw new Error('Invalid key type "%s" for adjustment'.format(type));

      let [lower, upper] = variant.deep_unpack();
      let adj = new Gtk.Adjustment({
        lower,
        upper,
        step_increment: step,
        page_increment: 10 * step
      });

      this._settings.bind(key, adj, "value", Gio.SettingsBindFlags.DEFAULT);

      return adj;
    }
  }
);

function buildPrefsWidget() {
  print(`buildPrefsWidget ${Me.metadata.name}`);

  let widget = new PrefsWidget();
  widget.show_all();

  return widget;
}
