# Tinge

A GNOME Shell extension to add a tinge of color to the whole screen.

![](./screenshots/prefs.png)

## Development Notes

_It's easier to develop in a Xorg/X11 session because changes to the extension require a restart of GNOME Shell and on Wayland that means you have to logout and back in._

To install for the first time and after making changes:

1. Run `make install`
2. [Restart GNOME Shell](#restarting-gnome-shell)
3. Enable the extension in GNOME Tweaks (should persist after restarts)

### Resources

- [General GNOME Shell extension info](https://wiki.gnome.org/Projects/GnomeShell/Extensions)
- [Writing GNOME Shell extensions](https://wiki.gnome.org/Projects/GnomeShell/Extensions/Writing)

### Restarting GNOME Shell

To restart GNOME Shell on Xorg/X11:

1. Press `ALT` + `F2`
2. Type `r` and enter

To restart GNOME Shell on Wayland:

1. Logout
2. Log back in
